<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Admin Admin",
            'user_name' => "admin",
            'email' => "admin@admins",
            'password' => bcrypt('123456'),
            'created_at' => time(),
            'updated_at' => time(),
        ]);
    }
}
