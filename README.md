

## Archivos trabajados

El api consta de los siguientes archivos

- rutas /routes/api.php.
- migraciones db /database/migrations/.
- controlador del api /app/Http/Controllers/AuthController.php.


Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Puesta en marcha

clonar repositorio

generar archivo de ambiente .env utilizando .env.example y configurar las siguientes lineas
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=basededatos
DB_USERNAME=usuariobase
DB_PASSWORD=passbase

luego en la carpeta raíz del proyecto ejecutar composer install esto instalará todas las dependencias necesarias

ejecutar php artisan migrate lo que generará las tablas correspondientes en la DB

ejecutar php artisan passport:install

ejecutar php artisan db:seed

