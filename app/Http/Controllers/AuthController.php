<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use App\User;

class AuthController extends Controller
{

    public function signUp(Request $request) {
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string'
        ]);

        User::create([
            'name' => $request->name,
            'user_name' => $request->user_name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        return response()->json([
            'message' => 'Usuario creado con éxito'
        ], 201);
    }

    public function usersImport(Request $request) {

        $uncomplete_ids = [];

        foreach($request->arreglo as $key => $user){
            if($key > 0) {
                if($user[0] && $user[1]) {
                    if(isset($user[2])) {
                        $mail = $user[2];
                    } else {
                        $mail = $user[0].'@'.$user[1].'.com';
                    }

                    $current_user = User::where("name", $user[1])->where("user_name", $user[0])->get();

                    if( !isset($current_user[0]) ) {
                        User::create([
                            'name' => $user[1],
                            'user_name' => $user[0],
                            'email' => $mail,
                            'password' => bcrypt('123456')
                        ]);
                    } else {

                    }

                } else {
                    array_push($uncomplete_ids, $key+1);
                }
            }
        }


        return response()->json([
            'message' => 'Ejecutado correctamente',
            'uncomplete' => json_encode($uncomplete_ids)
        ], 201);
    }

    public function login(Request $request) {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);

        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'No autorizado'
            ], 401);

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');

        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString()
        ]);
    }

    public function logout(Request $request) {
        $request->user()->token()->revoke();

        return response()->json([
            'message' => 'Sesión cerrada'
        ]);
    }

    public function users(Request $request) {
        $users_collection = User::get();
        return response()->json($users_collection);
    }
}
